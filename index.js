const http = require('http');
const addHeaders = (res) => {
    return res.setHeader('Content-Type', 'application/json');
}
const myError = (res, error = 'An unknown error occurred', statusCode = 500) => {
    addHeaders(res);
    res.statusCode = statusCode;
    res.end(JSON.stringify({
        status: 'fail',
        error
    }, null, 3));
};
const mySuccess = (res, data = null) => {
    addHeaders(res);
    res.statusCode = 200;
    res.end(JSON.stringify({
        status: 'success',
        data
    }, null, 3));
};


var jobStartedDate;
const mycron = require('cron').CronJob;
const job = new mycron(
                        '0 30 10 * * *',
                        loadData,
                        null,
                        false,
                        'Australia/Sydney'
                    );

class ProjectController {
    async refreshOnce (req, res) {
        try {
            await loadData();
            return mySuccess(res);
        }
        catch (error) {
            return myError(res, error);
        }
    }
    async startJob (req, res) {
        try {
          
            if(!jobStartedDate){
            	jobStartedDate = new Date();
                //job.start();
            }else{
            	throw "Job's already started @ " + jobStartedDate.toString();
            }

            return mySuccess(res);
        }
        catch (error) {
            return myError(res, error);
        }
    }
   async stopJob (req, res) {
        try {
            if(jobStartedDate){
            	jobStartedDate = null;
                //job.stop();
            }else{
            	throw "No running job to stop.";
            }
            return mySuccess(res);
        }
        catch (error) {
            return myError(res, error);
        }
    }

}

const projectController = new ProjectController();
const routes = [
    {
        method: 'GET',
        path: '/adminrefresh',
        handler: projectController.refreshOnce.bind(projectController)
    },
    {
        method: 'GET',
        path: '/startjob',
        handler: projectController.startJob.bind(projectController)
    },
    {
        method: 'GET',
        path: '/stopjob',
        handler: projectController.stopJob.bind(projectController)
    }

];

const router = async (req, res, routes) => {
    // Find a matching route
    const route = routes.find((route) => {
        const methodMatch = route.method === req.method;
        let pathMatch = false;

        if (typeof route.path === 'object') {
            // Path is a RegEx, we use RegEx matching
            pathMatch = req.url.match(route.path);
        }
        else {
            // Path is a string, we simply match with URL
            pathMatch = route.path === req.url;
        }

        return pathMatch && methodMatch;
    });

    // Extract the "id" parameter from route and pass it to controller
    let param = null;

    if (route && typeof route.path === 'object') {
        param = req.url.match(route.path)[1];
    }

    // Extract request body
    if (route) {
        let body = null;
        if (req.method === 'POST' || req.method === 'PUT') {
            body = await getPostData(req);
        }

        return route.handler(req, res, param, body);
    }
    else {
        return myError(res, 'Endpoint not found', 404);
    }
};


process.on('uncaughtException', function(err) {
    // handle the error safely
    console.log('uncaughtException');
    //console.error(err.stack);
    console.log(err);
});


const server = http.createServer(async (req, res) => {
    await router(req, res, routes);
});

var port = normalizePort(process.env.PORT || '3000');
function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

server.listen(port, () => {
    console.log('Server is listening on port 3000');
});


function loadData()
{
    var request = require('request');
    var https = require('https');

    let dt = new Date();
    console.log("\n\n\n\nStart pulling data @ " +dt.toISOString().slice(0,10) +" "+dt.toTimeString().slice(0,8));

    request.get("http://homepage.cs.latrobe.edu.au/smann/exposure.html", 
        function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const cheerio = require('cheerio');
            const $ = cheerio.load(body);

            const isAusPostcode = require('is-aus-postcode').default;

            $("tr").filter(i => i > 0).each(function(index,e){
                let spans = $(this).find('td:nth-of-type(2)').find('span');
                let text = "";
                for (let l = 0; l < spans.length; l++) {
                    if(!$(spans[l]).html().startsWith("<span")){
                        text += $(spans[l]).html();
                    }
                }
                let slines = text.replace(/\r\n/g, "\r").replace(/\n/g, " ").replace('<o:p></o:p>','').
				replace(/&#xA0;/g,' ').replace(/&apos;/g,'\'').replace(/&#xE9;/g,'e\'').split(/<br>/);
				
                //remove span inside of a span - ie "Off <span class="SpellE">Ya</span> Tree Watergardens"
                for (let l = 0; l < slines.length; l++) {
                    slines[l] = slines[l].trim().replace(/(<([^>]+)>)/gi, "");
                }

                let sitename, address;
                if(slines.length == 4){
                     sitename = slines[0];
                     address = slines[1]+"\n"+slines[2];
                }
                else if(slines.length == 3){
                     sitename = slines[0];
                     address = slines[1];
                }
                else if(slines.length == 1 && slines[0].indexOf(",")>0){
                     sitename = slines[0].split(",")[0];
                     address = slines[0].split(",")[1];
                }

                if(sitename){
                    let postcode = "";
                    if(slines.length > 1){
                        if(slines[slines.length -1].length > 3)
                            postcode = slines[slines.length -1].substr(slines[slines.length -1].length - 4);
                        if(!isAusPostcode(postcode)){
                            logger.error(`Row ${index} getting an invalid Australia postcode: ${postcode}`);
                            postcode = "";  
                        } 
                    }

                    let xpperiods = $(this).find('td:nth-of-type(3)').text().trim().split(/,/);
                    
                    for (let j = 0; j < xpperiods.length; j++) {
                        let elements = xpperiods[j].trim().split(/:|-|–|(am)|(AM)|(pm)|(PM)| |\/|\n|\r/g);      
                        let filtered = elements.filter(function (el) {
                            return el != null && el != ''; 
                        }); 
                        let d = {};
                        //converting start datetime
                        d.starthour = filtered[0]; 
                        filtered.splice(0,1);
                        if (filtered[0].toLowerCase() == 'am' || filtered[0].toLowerCase() == 'pm') {
                            d.startminute = "0";
                            d.startap = filtered[0];
                            filtered.splice(0,1);
                        }
                        else{
                            d.startminute = filtered[0];
                            d.startap = filtered[1];
                            filtered.splice(0,2);
                        }
                        // converting end datetime 
                        d.endhour = filtered[0]; 
                        filtered.splice(0,1);
                        if (filtered[0].toLowerCase() == 'am' || filtered[0].toLowerCase() == 'pm') {
                            d.endminute = "0";
                            d.endap = filtered[0];
                            filtered.splice(0,1);
                        }
                        else{
                            d.endminute = filtered[0];
                            d.endap = filtered[1];
                            filtered.splice(0,2);
                        }
                        d.date = filtered[0];
                        d.month = filtered[1];
                        d.year = filtered[2];

                        let startDate = new Date(`${d.month}/${d.date}/${d.year} ${d.starthour}:${d.startminute} ${d.startap}`);
                        let endDate = new Date(`${d.month}/${d.date}/${d.year} ${d.endhour}:${d.endminute} ${d.endap}`);


                        let data = JSON.stringify({
                          siteName: slines[0],
                          address: address,
                          suburbName: $(this).find('td:nth-of-type(1)').text().trim(),
                          postcode: postcode,
                          startDate: getDatetoString(startDate),
                          endDate: getDatetoString(endDate)
                        });
                        //console.log("\n\n"+data);
                        
                        const options = {
                          hostname: 'hotspotmapping-api-dev.azurewebsites.net',
                          port: 443,
                          path: '/api/Hotspot',
                          method: 'POST',
                          headers: {
                            'Content-Type': 'application/json',
                            'Content-Length': data.length
                          }
                        }
                        const req = https.request(options, res => {
                            logger.info(`\n\n statusCode: ${res.statusCode}`);
                            res.on('data', d => {
                                logger.info("\n\n"+data+"\n"+d);
                            })
                        })
                        req.on('error', error => {
                              logger.error(error);
                        })

                        req.write(data)
                        req.end()
                        
                    }       
                }
            });
            console.log("Reaching the end of the method @ " + (new Date()).toString());
        }
        else{
            console.error("Failed reading "+ url);
        }
    });
}

function getDatetoString(d){
    let s = d.getFullYear()
          + "-"
          + (d.getMonth()+1 < 10 ? '0' : '') + (d.getMonth()+1)
          + "-"
          + (d.getDate() < 10 ? '0' : '') + d.getDate()
          + "T"
          + d.toTimeString({hour12:false}).substr(0,8)
          + ".000Z";
    return s;
}